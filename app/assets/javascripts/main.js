window.handlersAppended = false;

$(document).ready(function() {

  var loadingAnimationFinished = false;
  var curr = 0;
  var fadingElements = $('.fade-in').sort(sortByDelay);
  var fadingInterval = setInterval(function() {
  var fadingElement = fadingElements[curr];
    if(fadingElement) {
      if(parseInt($(fadingElement).data('fade-delay')) === 2250) {
        $('.fade-in[data-fade-delay=2250]').addClass('done');
      }
      $(fadingElements[curr]).addClass('done');
      curr ++;
    } else {
      clearInterval(fadingInterval);
      loadingAnimationFinished = true;
    }
  }, 800);


  function sortByDelay(a, b){
    var aDel = parseInt($(a).data('fade-delay'));
    var bDel = parseInt($(b).data('fade-delay'));
    return ((aDel < bDel) ? -1 : ((aDel > bDel) ? 1 : 0));
  }
  if(!window.handlersAppended) {
    var PlayerState = {
      isMuted: true,
      isHidden: true,
      isPlaying: false,
      controlsHidden: false,
      numberOfVideos: 0,
      loaded: false
    };

    /*Vimeo Player/slider */
    var toggleClassesSound = ['assets/DBC_SOUNDOFF.svg', 'assets/DBC_SOUNDON.svg'],
      video = $('.slider li').find('.video').first(),
      volume_level = 0,
      video_state = 'paused',
      $pauseplay = $("#playpause"),
      $setvolume = $("#setvolume"),
      $viewcase = $('.viewcase'),
      $projectTitles = $('.project-title'),
      blockAnimation = false,
      caseLink = '#',
      allowControlsFade = !isMobile(),
      initialAutoplay = true,
      initialUnmute = true,
      autoplayOnSlideChange = !/chrome/.test(navigator.userAgent.toLowerCase()) && !isMobile(),
      autoplayOnSlideFinish = false;


    if (!!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform)) {
      $setvolume.css({display: 'none', opacity: '0'});
      $('#title').css({'margin-left': '-15px'});
    }

    if(video.length === 0 && $('.project-top iframe').length > 0) {
      $('#slider-wrapper').removeClass('loading');
    }

    if (video.length > 0) {
      var bx = $('.slider').bxSlider({
        onSliderLoad: function(){
          if(!window.handlersAppended) {
            window.handlersAppended = true;
            appendFinishHandlers();
          }
          var currSlideElement = $($('.slider li')[1]);
          caseLink = $viewcase.attr("href");
          $("#viewcase").attr("href", caseLink);
          updateTitles(currSlideElement);
        },
        onSlideBefore: function (slideElement, oldIndex, newIndex) {
          video.vimeo('unload');
          video.vimeo('pause');
          video_state = 'paused';
          PlayerState.controlsHidden = false;
          updateTitles(slideElement);
        },
        onSlideAfter: function (slideElement, oldIndex, newIndex) {
          video = slideElement.find('.video');
          video.vimeo("setVolume", 0);
          if (autoplayOnSlideChange || autoplayOnSlideFinish) {
            video_state = 'paused';
          } else {
            video_state = 'playing';
            PlayerState.controlsHidden = false;
          }
          togglePlayback();
          if (volume_level > 0) {
            volumeUp();
          }
          autoplayOnSlideFinish = false;

          $("#viewcase").attr("href", slideElement.find('.viewcase').attr("href"));
          // if(PlayerState.isHidden) {
          //   liftCurtain();
          //   PlayerState.isHidden = false;
          // }

        }
      });
    } else {
      $('.project-top iframe').on('playProgress',function (event, data) {
        if(data.percent > 0.98 || (data.duration-data.seconds < 1)) {
          if((volume_level === 0) && PlayerState.isHidden) {
            autoplayOnSlideFinish = true;
          }
          $(this).vimeo('unload');
          $(this).vimeo('pause');
          video_state = 'playing';
          PlayerState.controlsHidden = false;
          togglePlayback();
        }
      });
    }



    // Play / Pause toggle
    $pauseplay.on('click', function(){
      togglePlayback();
    });

    $setvolume.on('click', function(){
      toggleVolume();
    });


    function updateTitles(slide, onLoad) {
      var data = $(slide).find('.wrapper').data();
      if (onLoad) {
        $('#title').html(data.title);
        $('#title-prev').html(data.titlePrev);
        $('#title-next').html(data.titleNext);
        $projectTitles.fadeIn().css({display: 'inline-block'});
      } else {
        $projectTitles.fadeOut(100, function () {
          $('#title').html(data.title);
          $('#title-prev').html(data.titlePrev);
          $('#title-next').html(data.titleNext);
        }).fadeIn();
      }

    }

    function volumeUp() {
      var apiPlayer = new Vimeo.Player(video);
      apiPlayer.getVolume().then(function(currVolume) {
        var step = (1 - currVolume)/40;
        // var currVolume = volume;
        volume_level = 1;
        setTimeout(stepUp, 250);
        function stepUp() {
          // Check volume_level on each step to detect if sound was toggled
          if (currVolume < 1 && volume_level > 0) {
            currVolume =  currVolume + step;
            video.vimeo("setVolume", currVolume);
            setTimeout(stepUp, 200);
          }
        }
      });
    }


    $(window).scroll(function() {
      var $header = $('#header');
      if ($(this).scrollTop() > 120) {
        $header.fadeOut(215);
      } else {
        $header.fadeIn(511);
      }
    });

    $("#menu-toggle").click(function() {
      $('#myNav').toggle(1, function() {
        if($(this).is(':visible')) {
          $("body").addClass("modal-open");
          $("#menu-toggle").text("CLOSE");
        } else {
          $("#menu-toggle").text("MENU");
          $("body").removeClass("modal-open");
        }
      });
    });


    $(document).on("click", ".bottom-left", function(e) {
      e.preventDefault();
      bx.goToPrevSlide();
    });

    $(document).on("click", ".bottom-right", function(e) {
      e.preventDefault();
      bx.goToNextSlide();
    });

    $(document).on("click", ".youtube", function() {
      window.open("http://www.youtube.com/", "_blank")
    });
    $(document).on("click", ".facebook", function() {
      window.open("http://www.facebook.com/", "_blank")
    });
    $(document).on("click", ".soundcloud", function() {
      window.open("http://www.soundcloud.com/", "_blank")
    });

    $(document).on("click", ".overlay a", function() {
      $("html").removeClass("modal-open");
    });

    $(document).on("click", ".onhover", function() {
      var e = $(this).parents(".img-item").find("a").attr("href");
      return $(".gallery").fadeOut(1500, function() {
        window.location = e;
      });
    });

    $(document).on("click", "#closebtn", function(e) {
      lowerCurtain();
    });

    $(document).on("click", "#empty-space", function(e) {
      e.stopImmediatePropagation();
      $('#header').addClass('mobile-project');
      if (PlayerState.isHidden) {
        liftCurtain();

        hideControls();
        if(initialUnmute && volume_level === 0) {
          toggleVolume();
          initialUnmute = false;
        } else if (video_state !== 'playing') {
          togglePlayback();
        }
        $('#empty-space').css('cursor', 'default');
      }
    });
	  $(document).ready(function() {
     $('.marquee').marquee({
          duplicated: true
        });
     });

    $(window).mousemove(function () {
      if(!PlayerState.isHidden && !blockAnimation) {
        blockAnimation = true;
        showControls();
        if (video_state === 'playing') {
          setTimeout(function () {
            if(video_state === 'playing') {
              hideControls();
              blockAnimation = false;
            }
          }, 4000);
        } else {
          blockAnimation = false;
        }
      }
    });

    function lowerCurtain() {
      $('#header').removeClass('mobile-project');
      PlayerState.isHidden = true;
      if(PlayerState.controlsHidden) {
        showControls();
      }
      $('#curtain').slideToggle(2000);
      $('.navigation-project li').fadeOut(1000);
      $('.logo-right').removeClass('mobile-none');
      setTimeout(function () {
        $('.static').fadeIn(1000, function() {
          $('.navigation-project li').fadeOut(1000, function () {
            $('#empty-space').css('cursor', 'pointer');
            $('.logo-right').removeClass('mobile-hidden');
            $('.navigation-main li').fadeIn(1000);
          });
        });
      }, 1000);



      // $("#closebtn, #viewcase").fadeToggle(500);
      // $("#curtain").slideToggle(1500);
      // $(".static").fadeTo( 1500, 1 ).promise().then(function(){
      //   return $("#closebtn, #viewcase").fadeOut(800).promise();
      // })
      //   .done(function(){
      //     $(".nav").fadeIn(1400);
      //     $('#empty-space').css('cursor', 'pointer');
      //     $('#deskmenu').removeClass('done');
      //   });
    }

    function liftCurtain() {
      PlayerState.isHidden = false;
      $("#curtain").slideToggle(2000);
      $('.static').addClass('no-transition').fadeOut(1000);
      $('.logo-right').addClass('mobile-hidden');
      $('.navigation-main li').fadeOut(1000, function () {
        $('.logo-right').addClass('mobile-none');
        $('.navigation-project li').fadeIn(1000).css({display: 'inline-block'});
        $('#empty-space').css('cursor', 'default');
      });

      // $(".static").fadeTo( 2000, 0 ).promise().then(function(){
      //   return $(".nav").fadeToggle(1400).promise();
      // })
      //   .done(function(){
      //     $('#deskmenu').addClass('done');
      //     $("#closebtn, #viewcase").fadeToggle(500);
      //     $('#empty-space').css('cursor', 'default');
      //   });
    }

    function hideControls() {
      if (allowControlsFade && !PlayerState.isHidden && !PlayerState.controlsHidden) {
        PlayerState.controlsHidden = true;
        $('.bottom-left, .bottom-center, .bottom-right, #top-center, .project-title.top-left ,.project-title.top-right')
          .css({'opacity': 0, 'pointer-events': 'none'});
      }
    }

    function showControls() {
      if (allowControlsFade && PlayerState.controlsHidden) {
        PlayerState.controlsHidden = false;
        $('.bottom-left, .bottom-center, .bottom-right, #top-center, .project-title.top-left ,.project-title.top-right')
          .css({'opacity': 1,'pointer-events': 'auto'})
      }
    }

    function togglePlayback() {
      if (video.length > 0) {
        if (video_state === 'paused') {
          $pauseplay.attr('class', 'pause');
          video.vimeo('play');
          video_state = 'playing';
          // if(PlayerState.isHidden && !initialAutoplay && !autoplayOnSlideFinish) {
          //   liftCurtain();
          //   PlayerState.isHidden = false;
          // }
          if (!initialAutoplay) {

            hideControls();
          }
          initialAutoplay = false
        } else {
          $pauseplay.attr('class', 'play');
          video.vimeo('pause');
          video_state = 'paused';
          showControls();
        }
      }
      if ($('.project-top iframe').length > 0) {
        var projectVideo = $('.project-top iframe');
        if (video_state === 'paused') {
          $pauseplay.attr('class', 'pause');
          projectVideo.vimeo('play');
          video_state = 'playing';
        } else {
          $pauseplay.attr('class', 'play');
          projectVideo.vimeo('pause');
          video_state = 'paused';
          PlayerState.controlsHidden = false;
        }
      }
    }

    function toggleVolume() {
      if (volume_level !== 1) {
        $setvolume.attr('src', toggleClassesSound[1]);
        if (PlayerState.isHidden) {
          volumeUp();
        } else {
          video.vimeo("setVolume", 1);
          volume_level = 1;
        }
      } else {
        $setvolume.attr('src', toggleClassesSound[0]);
        video.vimeo("setVolume", 0);
        volume_level = 0;
      }
    }

    function appendFinishHandlers() {
      window.handlersAppended = true;
      $('.video').each(function (index, el) {
        $video = $(el);
        var $iframeContainer = $video.parent();
        var id = $video.data('video-id');
        PlayerState.numberOfVideos++;
        $video.remove();
        var $newVideo = $('<iframe></iframe>')
          .attr({
            "src": "https://player.vimeo.com/video/" + id + "?api=1&title=0&byline=0&portrait=0&transparent=0&color=ffffff&loop=1" + (index === 1 ? "&muted=1&autoplay=1" : ""),
            "frameborder": "0",
            "webkitallowfullscreen": "",
            "mozallowfullscreen": "",
            "allowfullscreen": "",
            "class": "video"
          });

        $iframeContainer.append($newVideo);
        $newVideo.vimeoLoad();


        $newVideo.on('playProgress',function (event, data) {
          if(data.percent > 0.98 || (data.duration-data.seconds < 1)) {
            if((volume_level === 0) && PlayerState.isHidden) {
              autoplayOnSlideFinish = true;
            }
            bx.goToNextSlide();
          }
        });
        if (index === 1) {
          video = $newVideo;
          video.on('ready', function() {
            if(!isMobile()) {
              video.vimeo('pause');
            }
            PlayerState.loaded = true;
          });
        }
      });
      var retryLimit = 10000;

      setTimeout(stopLoadingAnimation, 500);
      function stopLoadingAnimation() {
        if(!PlayerState.loaded) {
          setTimeout(stopLoadingAnimation, 100);
        } else {
          if(loadingAnimationFinished || (retryLimit === 0)) {
            $('#slider-wrapper').removeClass('loading');
            togglePlayback();
          } else {
            retryLimit = retryLimit - 250;
            setTimeout(stopLoadingAnimation, 250);
          }
        }
      }
    }

    function isMobile() {
      var check = false;
      (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
      return check;
    }


    //OVERLAY MENU/CLOSE

    // underline current nav menu
    $("#header li a").click(function() {
      $("#header li").removeClass("active");
      $(this).parent().addClass("active");
    });
  }
});




/*AJAXify app
1. transitions between pages
2. remove extra header elements on transitions + make sure scripts from "_header.html.erb" work
3. move projects (links, images, descriptions) & hardcoded youtube/facebook/etc links from ruby to json(?)
4. script for index page to insert prev/next projects titles in .top-left/.top-right
5. logic for projects without video
*/
