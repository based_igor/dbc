class AddAttachmentProjectimg2ToProjects < ActiveRecord::Migration
  def self.up
    change_table :projects do |t|
      t.attachment :projectimg2
    end
  end

  def self.down
    remove_attachment :projects, :projectimg2
  end
end
